import { z, defineCollection } from 'astro:content';

const pagesCollection = defineCollection({
	type: 'content',
	schema: z.object({
		title: z.string(),
		description: z.string(),
		body_class: z.string(),
	}),
});

const ridesCollection = defineCollection({
	type: 'data',
	schema: z.object({
		number: z.number(),
		date: z.date(),
		image: z.object({
			src: z.string(),
			alt: z.string(),
		}),
		ERurl: z.string().url(),
		FBurl: z.string().url(),
		isRidden: z.boolean(),
	}),
});

export const collections = {
	pages: pagesCollection,
	rides: ridesCollection,
};
