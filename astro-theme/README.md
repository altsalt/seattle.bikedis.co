# Bike Disco Theme

Bike Disco theme created using [`astro-theme-provider`](https://github.com/astrolicious/astro-theme-provider)!

## Install

1. Create an empty Astro project:

```sh
pnpm create astro@latest bikedisco-website --template minimal -y
```

2. Add the theme to your project:

```sh
pnpm astro add bikedisco-theme
```

## Configure

```ts
import { defineConfig } from "astro/config";
import BikeDiscoTheme from "bikedisco-theme";

export default defineConfig({
	integrations: [
		BikeDiscoTheme({
			config: {
				title: "??? Bike Disco",
				description: "Bike Disco in ???",
				feedback: "https://feedbacklink",
			},
		}),
	],
});

```

## Reference

[Astro Theme Provider documentation](https://astro-theme-provider.netlify.app/)
