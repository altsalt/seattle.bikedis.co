import defineTheme from "astro-theme-provider";
import { z } from "astro/zod";

export default defineTheme({
	name: "bikedisco-theme",
	imports: {
		styles: [
			"./src/styles/reset.css",
			"./src/styles/bsr-baseline.css",
			// './src/styles/utilities.css',
			"./src/styles/variables.css",
			"./src/styles/layout.css",
		],
	},
	schema: z.object({
		title: z.string(),
		description: z.string().optional(),
		feedback: z.string().url().optional(),
	}),
});
