import { defineConfig } from "astro/config";
import BikeDiscoTheme from "bikedisco-theme";

export default defineConfig({
	integrations: [
		BikeDiscoTheme({
			config: {
				title: "Bike Disco Site",
				description: "This site using the Bike Disco theme is currently under construction!",
				feedback: "https://forms.gle/",
			},
			pages: {
				"/": "index.astro",
			},
			overrides: {},
		}),
	],
});
