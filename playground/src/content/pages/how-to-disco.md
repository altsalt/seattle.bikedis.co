---
title: How To Disco /?/ Seattle Bike Disco
description: Rules and details the monthly SBD party is organized around.
ogtype: website
body_class: how-to-disco
# TODO: figure out how to apply particular css to md
# .how-to-disco .Page {
#   max-width: 45em;
# }
---

# WHAT’S A BIKE DISCO?

Monthly night ride and dance party. Light yourself, light your bike, and come party with us! We roll _rain or shine_.

## THE RIDE

_7:00pm_ - gather  
_7:30pm_ - roll out  
_Ride up to 5 miles_ - happy hour  
_Ride up to 5 miles_ - dance / supply stop  
_Ride up to 10 miles_ - party (~11:30pm&mdash;1:00am)  
_(see maps of previous rides on the gram-a-lam)_

_note_: Socials may not be updated as we go. Feel free to meet up or head out along the way; but please let the Party Caboose know before you peel so we don’t wait!


## HOW TO BIKE

1. ACQUIRE BIKE
	- Bring your bike, duh
	- If you don’t have a bike there are some excellent bike-share options
2. LIGHT YOUR BIKE
	- Don’t forget your headlight and taillight
3. HYDRATE
	- Don’t forget your water
4. HELMET
	- Protect ye olde noggin
5. DRESS FOR THE WEATHER
	- _All weather party_
	- We will potentially be out in _PNW_ cold and rainy weather, after dark, dress accordingly


## HOW TO DISCO

6. PARTY LIGHT YOURSELF + YOUR BIKE
	- Cover yourself and your bike in party lights (some examples below)
		- [Fairy Lights](https://www.amazon.com/gp/product/B0756WRTWH/)
		- [LED Strip Lights](https://www.amazon.com/gp/product/B07J2G4HQJ/)
		- [Disco Bike Light](https://www.amazon.com/gp/product/B01M3TWHJU/)
		- [Light up Gloves](https://www.amazon.com/gp/product/B01MAVOBWQ/)
7. COSTUME YOURSELF + YOUR BIKE
	- Costumes are encouraged, but not required.
	- Some months may have a theme.  Feel free to dress to the theme or however you like.
8. DE-HYDRATE
	- Bring a drink and snack for stops 1 &amp; 2, but expect a supply shop for BEvERages and snacks along the way.
9. MAKE SOME NOISE
	- Try to bring speakers that can be tuned to a safe volume while riding.  If they can connect to an internet-enabled device, we are working on a system to get everyone on the same playlist...


## STARTING ANNOUNCEMENTS

10. The People
	- _Who’s Who_: this ride’s Leader, Party Cabooses, Patch Kitten, and Shephard
	- _Buddies_: identify yours, don’t leave them behind
	- _Pictures_: please let us know if you want to opt-out
	- _Tune In_: use this [radio stream](http://seattle.bikedis.co/radio) for a shared soundtrack
11. Route Details
	- _Stops_: (un)known bathrooms, lack of lock-up at supply shop
	- _Not a round-trip_: find friends heading near your final destination
	- _Mobile dance with multiple stops_: when a 5 minute warning is called, find your bike to keep the party rolling
		- Keep an ear out for a puppet-icular song indicating we are riding soon
		- We try to start by group riding counter-clockwise around an object until the Ride Leader breaks off
	- Let the Party Caboose know if you’re peeling off
12. Rules of the Road
	- Keep a lane open when able; strive for fun and friendly, even to the metal boxes
		- (and do not ride against traffic...)
		- This is _not_ a protest ride
	- Ride Leader will ask for Corks / Plugs, please volunteer
		- Thank them for keeping us safe as you roll by!
	- Ride moves through lights as a train, once we start—keep rolling
	- Ride will slow/stop after turns and atop/abottom hills
		- You are encouraged to go at a pace that is safe for you on hills
		- ... but wait for and let the Ride Leader pass by if you get there first
	- Repeat yelled messages, if you hear it again, repeat it again
		- e.g., “Ped Up”, “Ballered”, “Pole”, “Left Turn”, “Car Back”
	- Yell “Mechanical” if you need the entire group to stop
		- This can be for bike mechanicals, body mechanicals, etc.
		- Ride Leader will identify a safe spot to pull off and call “Stopping”
		- Pull over as far as possible when coming to a stop, allow space for others
		- Organizers will assess the situation and decide whether to roll-on within 10 minutes
	- All participants must be on wheels while in motion
		- i.e., pets must be in a basket or trailer, not walking alongside
		- We travel at a bicycle pace and the Party Caboose tries to keep everyone above 10 mph on flats
	- Lower music volume to be overheard while in motion
		- Help maintain ride safety, yelled messages need to make it to each end
13. Consent and Kindness are Key
	- We are all here by choice
	- Respect each other’s space
	- Assume good intentions
	- Leave things better than they were


## HOW TO VOLUNTEER

14. Regular Organizers
	- Meet monthly to go over previous ride and plan the next route
	- Schedule quarterly ride dates on middle-Fridays based on organizer availability
15. Assigned Riders
	- _Ride Leader_: rides at start of the line making sure the ride stays together and on track
		- Leads opening announcements
		- Verifies Corkers / Plugs are in place before rolling through an unsafe intersection
		- Waits for Party Caboose signal before starting to roll
		- Should be very visible, from ahead and behind
		- Pre-rides route at least a week beforehand
	- _Patch Kitten_: rides between front and back of line while in motion
		- Carries basic bike repair equipment
		- Works with Paratrooper to resolve mechanicals
		- Rushes messages between Ride Leader and Party Caboose if radios not working
	- _Shepherd_: rides next to line keeping everyone in one lane
		- Makes sure there is enough space for Patch Kitten and Corkers to pass
	- _Paratrooper_: rides at the back of the line to support a mechanical splinter group
		- Communicates mechanical details over the radio and to Patch Kitten if Ride Leader unreachable
		- Takes 5 minutes to decide whether a mechanical will take longer than 10 minutes to resolve
		- If splitting, gathers a small group to stay with the mechnical and communicates the catch-up spot
		- Cabooses the splinter group until reconnected with the main line
	- _Party Caboose_: rides at back of the line making sure no one is dropped
		- Cheers on slower riders and communicates if things need to slow down
		- Blocks any vehicles from passing for line safety
		- Should be very visible, from ahead and behind
		- Pre-rides route at least a week beforehand
	- _DJ_: changes shared playlist during ride to match the needed vibes
16. Mid-ride Volunteers
	- _Corks / Plugs_: stops and secures intersections by request of the Ride Leader
		- Having more than one per intersection side is always nice
		- Sometimes shines a light on an obstical or tricky turn
		- Waits for Party Caboose to arrive before rejoining ride
	- _Jukeboxes_: brings speakers and plays tunes, while riding and dancing
		- Keeps volume at a reasonable level for the situation
	- _Photographers_: captures the many moments of fun we build together
		- Everyone can participate, but nice to have at least one person committed to this each ride
		- Media will be collected, curated, and displayed on the website... eventually
17. Off-road Laborers
	- _Archon_: creates meeting documents and updates the shared calendar
	- _Cartographer_: designs the route map graphic for release on the week of the ride
	- _Social Media Maven_: creates the ride event pages on various Internet platforms
		- Responds to messages coming in on the various platforms
