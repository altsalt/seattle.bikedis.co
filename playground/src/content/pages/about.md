---
title: About /?/ Seattle Bike Disco
description: Links and resouces for the community of local SBD cyclists.
ogtype: website
body_class: about
---

## Social Media
“Official” places we can be found ’round the ’net.
- [Everyday Rides](https://everydayrides.com/groups/seattle-bike-disco)
- [Facebook](https://www.facebook.com/seattlebikedisco)
- [Instagram](https://www.instagram.com/seattlebikedisco)
- [Love To Ride](https://www.lovetoride.net/washington/groups/2098)
- [Strava](https://www.strava.com/clubs/960747)
- [Twitter](https://twitter.com/SeaBikeDisco)
- `#seattle-bike-disco` on the [Seattle Cyclist Discord](https://tinyurl.com/seattlecyclist)

```
#Bicycles #BikeCostumes #BikeDanceParty #BikeDisco
#BikeLife #BikeParty #Bikes #BikeSeattle #DanceParty
#Disco #FridayNightRide #GroupRide #NightRide #PartyPace
#PNWBikeLife #RideBikes #RollingDanceParty #Seattle
#SeattleBikeDisco #SeattleBikes
#iwanttoridemybicycle
#youwanttorideyourbicycle
```

## Current (dis)Organizers
Meeting monthly to keep the ride rolling.
- Jayson
- Julian
- Kaleena
- Kathryn
- Peaches
- Salt
- Tor

## History
Coming Soon?

## Seattle Group Rides
Other local orgs that regularly lead rides.  
Not an endorsement or recommendation, merely information sharing.  
See some we missed? Let us know!
- [All Bodies on Bikes (ABOB)](https://linktr.ee/abob_seattle)
- [Ampersand Bikes Club (&BC/ABC) [BIPOC]](https://www.instagram.com/abc.seattle/)
- [Bike Works Social Rides](https://bikeworks.org/calendar/)
- [The Bikery Social Rides [LGBTQ+]](https://www.thebikery.org/socialrides)
- [Bike Happy Hour](https://www.instagram.com/bikehappyhour)
- [Breakfast Racing Team [FTW-NB]](https://www.breakfastcycling.club/)
- [Cascade [Free Group Rides]](https://cascade.org/rides-events/ride-event-search?f%5B0%5D=event_type%3A21)
- [Coffee Outside](https://www.instagram.com/coffeeoutsidesea)
- [Critical Mass](http://bit.ly/seattleCM)
- [Cyclists of Greater Seattle (COGS)](https://cyclistsofgreaterseattle.wildapricot.org/)
- [Dead Baby Bikes](http://www.deadbabybikes.org/)
- [Emerald City Bike Party](https://www.instagram.com/ecbikeparty/)
- [Evergreen Gravel Racing](https://evergreengravelracing.wordpress.com/)
- [Friends On Bikes (FOB) [FTW-NB]](https://www.instagram.com/fob_sea/)
- [Good Weather Fun Club](https://everydayrides.com/groups/good-weather-fun-club)
- [Moxie Monday [FTW-NB]](https://www.instagram.com/moxiemonday)
- [Northstar Cycling Club [BIPOC]](https://www.northstarcycling.org/)
- [North End Social Ride](https://www.instagram.com/northendsocialride/)
- [Peace Peloton](https://www.peacepeloton.com/)
- [Pedaling Relief Project](https://cascade.org/outreach-advocacy/pedaling-relief-project)
- [Point 83 (.83)](https://point83.com/)
- [Seattle Randonneurs (SIR)](https://www.seattlerando.org/)
- [World Naked Bike Ride (WNBR) Seattle](https://www.facebook.com/WNBRSeattle)

## Seattle Bike Links
Other online resources that local cyclists may find useful.  
Once again, not an endorsement or recommendation, merely information sharing.  
See some we missed? Let us know!
- [Commute Seattle Bike Resources](https://www.commuteseattle.com/resource/bike-resources/)
- [Everyday Rides Calendar](https://everydayrides.com/calendar)
- [SDOT Bike Web Map](https://www.seattle.gov/transportation/projects-and-programs/programs/bike-program/bike-web-map)
- [Seattle Bike Blog](https://www.seattlebikeblog.com/)
- [Seattle Neighborhood Greenways](https://seattlegreenways.org/)
- [UnGapTheMap](https://ungapthemap.seattlegreenways.org/)
- [WSDOT Bike Routes](https://wsdot.wa.gov/travel/bicycling-walking/bicycling-washington/us-bike-routes)
