{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
	nativeBuildInputs = with pkgs; [
		nodejs_22
		(pnpm_9.override { nodejs = nodejs_22; })
		biome
		playwright-driver.browsers
	];

	shellHook = ''
		export BIOME_BINARY="${pkgs.biome}/bin/biome"
		export PLAYWRIGHT_BROWSERS_PATH=${pkgs.playwright-driver.browsers}
	'';
}
