import { defineConfig } from "astro/config";
import BikeDiscoTheme from "bikedisco-theme";

export default defineConfig({
	site: "https://seattle.bikedis.co",
	integrations: [
		BikeDiscoTheme({
			config: {
				title: "Seattle Bike Disco",
				description: "Seattle Bike Disco: inclusive, friendly, and zany; no-drop non-stop dance party on wheels!!",
				feedback: "https://seattle.bikedis.co/feedback",
			},
			pages: {
				"/": "index.astro",
			},
			overrides: {},
		}),
	],
});
