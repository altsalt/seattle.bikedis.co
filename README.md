# Seattle Bike Disco

Official website for the Seattle Bike Disco!

Developed as an Astro theme integration built with [`Astro Theme Provider`](https://github.com/astrolicious/astro-theme-provider).

Deploys to GitLab pages.

## Usage

1. Clone the repo

```sh
git clone git@gitlab.com:altsalt/seattle.bikedis.co.git
cd seattle.bikedis.co/
```

2. Setup environment

Basic example on NixOS:

```sh
# shellcheck shell=bash
use nix
```

3. Install dependencies

> **Note:** This project uses **_[pnpm](https://github.com/pnpm/pnpm)_** workspaces, you must use **_pnpm_** as your package manager.

```sh
direnv allow
pnpm install
```

4. Begin developing

> **Important**: The playground always has to be running to generate types when modifying the theme

```sh
pnpm playground:dev
```

Explore the repo, check the [Astro Theme Provider Docs](https://astro-theme-provider.netlify.app/), and [Astro Docs](https://docs.astro.build/en/getting-started/).


## Reference

- [Astro Docs](https://docs.astro.build/en/getting-started/)
	- [Structure](https://docs.astro.build/en/basics/project-structure/)
	- [Components](https://docs.astro.build/en/basics/astro-components/)
	- [Layouts](https://docs.astro.build/en/basics/layouts/)
	- [Template Syntx](https://docs.astro.build/en/basics/astro-syntax/)
	- [Content Collections](https://docs.astro.build/en/guides/content-collections/)
	- [Migrate to Astro](https://docs.astro.build/en/guides/migrate-to-astro/)
	- [Migrating from Gatsby](https://docs.astro.build/en/guides/migrate-to-astro/from-gatsby/)
	- [Routing](https://docs.astro.build/en/guides/routing/)
	- [Styling](https://docs.astro.build/en/guides/styling/)
	- [Images](https://docs.astro.build/en/guides/images/)
	- [Imports](https://docs.astro.build/en/guides/imports/)
	- [Config File](https://docs.astro.build/en/guides/configuring-astro/)
	- [Integration API](https://docs.astro.build/en/reference/integrations-reference/)

- [Atro Theme Provider](https://github.com/astrolicious/astro-theme-provider)
	- [Template](https://github.com/astrolicious/astro-theme-provider-template)
	- [Docs](https://astro-theme-provider.netlify.app/)

- [Astro UI Book](https://www.freecodecamp.org/news/how-to-use-the-astro-ui-framework)
	- [GitHub](https://github.com/understanding-astro)

- [Astro integrations](https://astro.build/integrations/)
	- https://docs.astro.build/en/guides/integrations-guide/partytown/
	- https://docs.astro.build/en/guides/integrations-guide/mdx/
	- https://docs.astro.build/en/guides/integrations-guide/sitemap/
	- https://docs.astro.build/en/guides/integrations-guide/tailwind/
	- https://github.com/jonasmerlin/astro-seo

- [Astro Integration Kit](https://astro-integration-kit.netlify.app/)


## Bikeshed

> the things that don't have a place yet, but just can't be thrown out

### planning notes
- Salt would like to get something basic up but needs some guidance
- Sections being considered so far:
	- how to disco
	- gallery
	- about / history / organizers
		- links to all social accounts
		- other group rides in seattle
	- merchandise
- design thoughts:
	- logo front/center/large
	- quarter’s ride calendars below, with links to various social pages
	- top menu with links to sections above
	- link to anonymous feedback form below other content on all pages
